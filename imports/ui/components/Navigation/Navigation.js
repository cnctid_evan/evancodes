import React from 'react';
import autoBind from 'react-autobind';
import { Navbar, NavItem, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom'; // Tools to route our users to the correct pages

import './Navigation.scss';

// Can probably use props to highlight active page correctly
class Navigation extends React.Component {
  constructor(props){
    super(props);
    this.state = { activePage: '/' };
    autoBind(this);
  }

  render() {
    return(
      <div className="Navigation">
        <div className="mynavbar">
          <div className="mynavbar-header">
            <a className="mynavbar-brand" href="/">Evan McCullough</a>
          </div>
        </div>
      </div>
    );
  }
}

export default Navigation;
