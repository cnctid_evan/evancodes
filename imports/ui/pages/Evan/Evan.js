import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import './Evan.scss';

class Evan extends React.Component {
  componentDidMount() {
    this.changeSubGreeting();
    setInterval(this.changeSubGreeting, 15000);
  }

  changeSubGreeting() {
    const sayings = [
      'I have 8 cats',
      'I love great design and make "kindergarten art"',
      'I love pro wrestling (and that\'s the bottom line)',
      'I integrate technology in the classroom for Granville Schools',
      'I am working to build my skills and portfolio in web development',
      'I don\'t trust Alexa',
      'I listen to lots of podcasts (about wrestling)',
      'I spend at least 30% of my time dreaming up ideas',
      'I am a problem-solver',
      'I am a better learner since becoming a teacher',
      'I\'m more of a jeans guy than a dress clothes guy',
      'I wear lots of plaid',
      'I am probably feeding my cats',
      'I am probably cleaning cat litter'
    ]

    const sayings_length = sayings.length;
    const rand_index = Math.floor(Math.random() * (sayings_length-1));
    const rand_saying = sayings[rand_index];
    const sub_greeting_div = document.getElementsByClassName("sub-greeting")[0];
    sub_greeting_div.innerHTML = rand_saying;
  }

  render() {
    return(
      <div className="Evan">
        <Grid fluid>
          <Row>
            <Col xs={8} sm={6} xsOffset={2} smOffset={3}>
              <div className="greeting">Hi, I'm Evan</div>
            </Col>
          </Row>
          <Row>
            <Col xs={8} sm={6} xsOffset={2} smOffset={3}>
              <div className="sub-greeting"></div>
            </Col>
          </Row>

        </Grid>
      </div>
    );
  }
}


export default Evan;
