import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'; // Tools to route our users to the correct pages
import { Grid } from 'react-bootstrap';

import Navigation from '../../components/Navigation/Navigation';
import Evan from '../../pages/Evan/Evan';

import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props); // Pass props given to this up to parent React Component.
  }

  render() {
    return(
      <Router>
        <div className="App">
          <Grid fluid>
            <Navigation />
            <Switch>
              <Route exact path="/" component={Evan} />
              <Route exact path="/Husband" component={Evan} />
              <Route exact path="/Cats" component={Evan} />
              <Route exact path="/Daydreams" component={Evan} />
              <Route exact path="/Technologist" component={Evan} />
              <Route exact path="/Educator" component={Evan} />
              <Route exact path="/Developer" component={Evan} />
            </Switch>
          </Grid>
        </div>
      </Router>
    );
  }
}

export default App;
