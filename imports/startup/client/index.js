import React from 'react';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';
import App from '../../ui/layouts/App/App';
import '../both/api'; // Pull shared API (calls that can be made from client or server)

import '../../ui/stylesheets/app.scss'; // Import our app's general styles.

Meteor.startup(() => render(<App />, document.getElementById('react-root'))); // Render the App in the div '#react-root'
