var require = meteorInstall({"client":{"template.main.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// client/template.main.js                                                                                   //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //

Template.body.addContent((function() {
  var view = this;
  return HTML.Raw('<div id="react-root"></div>');
}));
Meteor.startup(Template.body.renderToDocument);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// client/main.js                                                                                            //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.watch(require("../imports/startup/client"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"imports":{"ui":{"components":{"Navigation":{"Navigation.js":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/components/Navigation/Navigation.js                                                            //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
let React;
module.watch(require("react"), {
  default(v) {
    React = v;
  }

}, 0);
let autoBind;
module.watch(require("react-autobind"), {
  default(v) {
    autoBind = v;
  }

}, 1);
let Navbar, NavItem, Nav;
module.watch(require("react-bootstrap"), {
  Navbar(v) {
    Navbar = v;
  },

  NavItem(v) {
    NavItem = v;
  },

  Nav(v) {
    Nav = v;
  }

}, 2);
let Link;
module.watch(require("react-router-dom"), {
  Link(v) {
    Link = v;
  }

}, 3);
module.watch(require("./Navigation.scss"));

// Can probably use props to highlight active page correctly
class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: '/'
    };
    autoBind(this);
  }

  render() {
    return React.createElement("div", {
      className: "Navigation"
    }, React.createElement("div", {
      className: "mynavbar"
    }, React.createElement("div", {
      className: "mynavbar-header"
    }, React.createElement("a", {
      className: "mynavbar-brand",
      href: "/"
    }, "Evan McCullough"))));
  }

}

module.exportDefault(Navigation);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"Navigation.scss":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/components/Navigation/Navigation.scss                                                          //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.watch(require("./Navigation.scss.css"), {
  "*": module.makeNsSetter(true)
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"Navigation.scss.css":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/components/Navigation/Navigation.scss.css                                                      //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.exports = require("meteor/modules").addStyles(
  "@import url(\"https://fonts.googleapis.com/css?family=Lato|Open+Sans|Oswald|Sunflower:300\");\n@import url(\"https://use.typekit.net/ifa3iub.css\");\n.Navigation {\n  color: white;\n  font-family: \"Bebas Neue\", sans-serif; }\n\n.mynavbar {\n  background-color: transparent;\n  padding-left: 3%;\n  padding-right: 3%; }\n\n.mynavbar-brand {\n  color: white;\n  font-size: 2.5em; }\n"
);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"layouts":{"App":{"App.js":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/layouts/App/App.js                                                                             //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
let React;
module.watch(require("react"), {
  default(v) {
    React = v;
  }

}, 0);
let Router, Switch, Route;
module.watch(require("react-router-dom"), {
  BrowserRouter(v) {
    Router = v;
  },

  Switch(v) {
    Switch = v;
  },

  Route(v) {
    Route = v;
  }

}, 1);
let Grid;
module.watch(require("react-bootstrap"), {
  Grid(v) {
    Grid = v;
  }

}, 2);
let Navigation;
module.watch(require("../../components/Navigation/Navigation"), {
  default(v) {
    Navigation = v;
  }

}, 3);
let Evan;
module.watch(require("../../pages/Evan/Evan"), {
  default(v) {
    Evan = v;
  }

}, 4);
module.watch(require("./App.scss"));

class App extends React.Component {
  constructor(props) {
    super(props); // Pass props given to this up to parent React Component.
  }

  render() {
    return React.createElement(Router, null, React.createElement("div", {
      className: "App"
    }, React.createElement(Grid, {
      fluid: true
    }, React.createElement(Navigation, null), React.createElement(Switch, null, React.createElement(Route, {
      exact: true,
      path: "/",
      component: Evan
    }), React.createElement(Route, {
      exact: true,
      path: "/Husband",
      component: Evan
    }), React.createElement(Route, {
      exact: true,
      path: "/Cats",
      component: Evan
    }), React.createElement(Route, {
      exact: true,
      path: "/Daydreams",
      component: Evan
    }), React.createElement(Route, {
      exact: true,
      path: "/Technologist",
      component: Evan
    }), React.createElement(Route, {
      exact: true,
      path: "/Educator",
      component: Evan
    }), React.createElement(Route, {
      exact: true,
      path: "/Developer",
      component: Evan
    })))));
  }

}

module.exportDefault(App);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"App.scss":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/layouts/App/App.scss                                                                           //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.watch(require("./App.scss.css"), {
  "*": module.makeNsSetter(true)
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"App.scss.css":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/layouts/App/App.scss.css                                                                       //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.exports = require("meteor/modules").addStyles(
  ".App > .container-fluid {\n  padding-right: 0px;\n  padding-left: 0px; }\n"
);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"pages":{"Evan":{"Evan.js":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/pages/Evan/Evan.js                                                                             //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
let React;
module.watch(require("react"), {
  default(v) {
    React = v;
  }

}, 0);
let Grid, Row, Col;
module.watch(require("react-bootstrap"), {
  Grid(v) {
    Grid = v;
  },

  Row(v) {
    Row = v;
  },

  Col(v) {
    Col = v;
  }

}, 1);
module.watch(require("./Evan.scss"));

class Evan extends React.Component {
  componentDidMount() {
    this.changeSubGreeting();
    setInterval(this.changeSubGreeting, 15000);
  }

  changeSubGreeting() {
    const sayings = ['I have 8 cats', 'I love great design and make "kindergarten art"', 'I love pro wrestling (and that\'s the bottom line)', 'I integrate technology in the classroom for Granville Schools', 'I am working to build my skills and portfolio in web development', 'I hate technical interviews', 'I don\'t trust Alexa', 'I listen to lots of podcasts (about wrestling)', 'I spend at least 30% of my time dreaming up ideas', 'I am a problem-solver', 'I am a better learner since becoming a teacher', 'I hate dress clothes', 'I wear lots of plaid', 'I am probably feeding my cats', 'I am probably cleaning cat litter'];
    const sayings_length = sayings.length;
    const rand_index = Math.floor(Math.random() * (sayings_length - 1));
    const rand_saying = sayings[rand_index];
    const sub_greeting_div = document.getElementsByClassName("sub-greeting")[0];
    sub_greeting_div.innerHTML = rand_saying;
  }

  render() {
    return React.createElement("div", {
      className: "Evan"
    }, React.createElement(Grid, {
      fluid: true
    }, React.createElement(Row, null, React.createElement(Col, {
      xs: 8,
      sm: 6,
      xsOffset: 2,
      smOffset: 3
    }, React.createElement("div", {
      className: "greeting"
    }, "Hi, I'm Evan"))), React.createElement(Row, null, React.createElement(Col, {
      xs: 8,
      sm: 6,
      xsOffset: 2,
      smOffset: 3
    }, React.createElement("div", {
      className: "sub-greeting"
    })))));
  }

}

module.exportDefault(Evan);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"Evan.scss":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/pages/Evan/Evan.scss                                                                           //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.watch(require("./Evan.scss.css"), {
  "*": module.makeNsSetter(true)
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"Evan.scss.css":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/pages/Evan/Evan.scss.css                                                                       //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.exports = require("meteor/modules").addStyles(
  "@import url(\"https://fonts.googleapis.com/css?family=Lato|Open+Sans|Oswald|Sunflower:300\");\n@import url(\"https://use.typekit.net/ifa3iub.css\");\n.Evan {\n  height: 98vh;\n  background: linear-gradient(rgba(32, 31, 38, 0.5), rgba(32, 31, 38, 0.5)), url(\"/emma+evan(highres)-82.jpg\");\n  color: white; }\n  @media (min-width: 320px) {\n    .Evan {\n      background-size: 200%;\n      background-repeat: no-repeat;\n      background-position: 50% 0%; } }\n  @media (min-width: 480px) {\n    .Evan {\n      background-size: 100%;\n      background-repeat: no-repeat;\n      background-position: 50% 0%; } }\n  @media (min-width: 768px) {\n    .Evan {\n      background-size: 100%;\n      background-repeat: no-repeat;\n      margin-top: -50px; } }\n\n.Evan > .container-fluid {\n  padding-left: 0px;\n  padding-right: 0px; }\n\n.greeting, .sub-greeting {\n  animation-name: fade-in;\n  animation-fill-mode: both;\n  animation-duration: 3s;\n  animation-delay: 1s; }\n\n@keyframes fade-in {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n\n.greeting {\n  display: flex;\n  justify-content: center;\n  text-align: center;\n  font-family: \"Oswald\", \"Lato\", sans-serif; }\n  @media (min-width: 320px) {\n    .greeting {\n      padding-top: 25vh;\n      font-size: 3.0em; } }\n  @media (min-width: 480px) {\n    .greeting {\n      padding-top: 30vh; } }\n  @media (min-width: 768px) {\n    .greeting {\n      padding-top: 40vh;\n      font-size: 4.0em; } }\n\n.sub-greeting {\n  display: flex;\n  justify-content: center;\n  text-align: center;\n  font-family: \"Sunflower\", \"Open Sans\", serif; }\n  @media (min-width: 320px) {\n    .sub-greeting {\n      font-size: 1.25em; } }\n  @media (min-width: 768px) {\n    .sub-greeting {\n      font-size: 2.0em; } }\n"
);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"stylesheets":{"app.scss":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/stylesheets/app.scss                                                                           //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.watch(require("./app.scss.css"), {
  "*": module.makeNsSetter(true)
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"app.scss.css":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/ui/stylesheets/app.scss.css                                                                       //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
module.exports = require("meteor/modules").addStyles(
  "@import url(\"https://fonts.googleapis.com/css?family=Lato|Open+Sans|Oswald|Sunflower:300\");\n@import url(\"https://use.typekit.net/ifa3iub.css\");\n"
);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"startup":{"both":{"api.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/startup/both/api.js                                                                               //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"client":{"index.js":function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// imports/startup/client/index.js                                                                           //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
let React;
module.watch(require("react"), {
  default(v) {
    React = v;
  }

}, 0);
let render;
module.watch(require("react-dom"), {
  render(v) {
    render = v;
  }

}, 1);
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 2);
let App;
module.watch(require("../../ui/layouts/App/App"), {
  default(v) {
    App = v;
  }

}, 3);
module.watch(require("../both/api"));
module.watch(require("../../ui/stylesheets/app.scss"));
// Import our app's general styles.
Meteor.startup(() => render(React.createElement(App, null), document.getElementById('react-root'))); // Render the App in the div '#react-root'
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}}}},{
  "extensions": [
    ".js",
    ".json",
    ".html",
    ".scss",
    ".css"
  ]
});
require("/client/template.main.js");
require("/client/main.js");