#EvanCodes
evancodes is the beginning framework for a Meteor-based personal portfolio
site. Thus far, it is only home to a base webpage with a large photo of me and
a collection of little personal tidbits that cycle through every 15 seconds.
In time, I plan to add links, screenshots, and descriptions of some of my work.

For now, though, you can check that work out through my
[GitHub profile](https://github.com/bigred053).

##Test it out
If you would like to test out what I have here so far, you can clone this
repo, cd into the evancodes directory, and then use this command:

```
meteor
```

This will initialize the application and start it running at localhost:3000.

##Why Meteor?
I have played around with Meteor on a couple of small projects over the past
6-8 months and have really loved working with the framework. I love the
emphasis that Meteor places on highly available, up-to-date content, and also
the idea of programming everything in a single language (JavaScript). Meteor
also includes many useful development features, such as active reload.

As I have worked with Meteor, I have also taken the opportunity to learn to
work with React through the ReactBootstrap library. I love the combination
of content and logic all within a single React component.
